
name := "sbt-formdefinition-plugin"

organization := "com.uxforms"

sbtPlugin := true

libraryDependencies ++= Seq(
  "org.dispatchhttp" %% "dispatch-core" % "1.2.0",
  "com.github.spullara.mustache.java" % "compiler" % "0.9.7",
  "com.typesafe" % "config" % "1.4.1",
  "com.lihaoyi" %% "upickle" % "2.0.0",
  "commons-io" % "commons-io" % "2.11.0",
  "org.yaml" % "snakeyaml" % "1.25" % Test,
  "org.mockito" %% "mockito-scala" % "1.17.12" % Test
)

addSbtPlugin("com.typesafe.sbt" % "sbt-osgi" % "0.9.6")

publishMavenStyle := true

publishTo := Some("uxforms-public" at "s3://artifacts-public.uxforms.net")

licenses += ("MIT", url("http://opensource.org/licenses/MIT"))

libraryDependencies += "com.lihaoyi" %% "utest" % "0.7.2" % "test"

testFrameworks += new TestFramework("utest.runner.Framework")