package com.uxforms.sbt

import upickle.default.{macroRW, ReadWriter as RW}

case class DockerHubResult(name: String) {

  lazy val tag: Option[Tag] = name.split("-dsl.").toList match {
    case (a :: b :: Nil) => for {
      formExecutorVersion <- SemanticVersion(a)
      dslVersion <- SemanticVersion(b)
    } yield Tag(s"$a-dsl.$b", formExecutorVersion, dslVersion)
    case _ => None
  }
}

object DockerHubResult {
  implicit val dockerHubResultRW: RW[DockerHubResult] = macroRW
}

case class DockerHubResponse(next: String, results: List[DockerHubResult]) {
  lazy val hasNext: Boolean = Option(next).exists(_.nonEmpty)
}

object DockerHubResponse {
  implicit val dockerHubResponseRW: RW[DockerHubResponse] = macroRW
}