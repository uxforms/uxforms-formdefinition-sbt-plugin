package com.uxforms.sbt

case class Tag(name: String, formexecutorVersion: SemanticVersion, dslVersion: SemanticVersion)
