package com.uxforms.sbt

import java.nio.charset.StandardCharsets

import sbt.{File, Logger}

object Deploy2 {

  import dispatch._
  import Defaults._

  import scala.concurrent.duration._

  def apply(formDeployerHost: String, apikey: String, formFile: File, name: String, version: String, activate: Boolean)(logger: Logger): Unit = {
    val url = s"$formDeployerHost/form"
    logger.debug(s"Posting $formFile to $url")

    val deployerUrl = dispatch.url(url)
      .addHeader("Authorization", s"""UXFORMS-TOKEN apikey="$apikey"""")
      .addHeader("Content-Type", "application/java-archive")
      .addQueryParameter("activate", BooleanHelper.toNiceString(activate))
    val response = scala.concurrent.Await.result(Http.default(deployerUrl.setBody(formFile.getAbsoluteFile).POST), 180.seconds)
    response.getStatusCode match {
      case 200 => logger.info(s"Successfully deployed $name version $version to $url")
      case 202 => logger.info(s"Successfully deployed $name version $version to $url")
      case 400 => throw InvalidFormFileException(response.getResponseBody(StandardCharsets.UTF_8))
      case 401 => throw PermissionDeniedException
      case 409 => throw FormAlreadyDeployedException(name, version)
      case c@_ => logger.error(s"Returned $c with ${response.getResponseBody(StandardCharsets.UTF_8)} from $url")
    }
  }
}
