package com.uxforms.sbt

import java.nio.charset.StandardCharsets

import sbt.Logger

object Undeploy2 {

  import dispatch._
  import Defaults._

  import scala.concurrent.duration._

  def apply(formDeployerHost: String, apikey: String, name: String, version: String, force: Boolean)(logger: Logger): Boolean = {
    val url = s"$formDeployerHost/form/$name/$version"
    logger.debug(s"Deleting $url")

    val deployerUrl = dispatch.url(url)
      .addHeader("Authorization", s"""UXFORMS-TOKEN apikey="$apikey"""")
      .addQueryParameter("force", BooleanHelper.toNiceString(force))
    val response = scala.concurrent.Await.result(Http.default(deployerUrl.DELETE), 60.seconds)
    response.getStatusCode match {
      case 200 =>
        logger.info(s"Successfully deleted $name version $version from $url")
        true
      case 401 => throw PermissionDeniedException
      case 404 =>
        logger.warn(s"No such form $name with version $version on $url")
        false
      case c@_ =>
        logger.error(s"Returned $c with ${response.getResponseBody(StandardCharsets.UTF_8)} from $url")
        false
    }
  }
}
