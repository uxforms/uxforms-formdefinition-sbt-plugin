package com.uxforms.sbt

import java.util.Properties

import sbt.{File, IO, Logger}

object DeployProperties {

  def apply(propertiesFileLocation: String)(logger: Logger): Properties = {
    logger.debug(s"Loading properties from $propertiesFileLocation")
    val prop = new Properties()
    IO.load(prop, new File(propertiesFileLocation))
    prop
  }
}
