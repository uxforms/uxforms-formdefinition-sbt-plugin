package com.uxforms.sbt

import dispatch.*
import dispatch.Defaults.*
import upickle.default.read

import java.nio.charset.StandardCharsets
import scala.concurrent.Await
import scala.concurrent.duration.*
import scala.util.Try

object ImageFinder {

  def findAllTags(): Set[Tag] = {

    def fetchTags(endpoint: String): Set[Tag] = {
      val dockerHub = url(endpoint)
      val response = Await.result(Http.default(dockerHub.GET), 30 seconds)
      val body = response.getResponseBody(StandardCharsets.UTF_8)
      val parsed = read[DockerHubResponse](body)
      val tags = parsed.results.flatMap(_.tag).toSet
      if (parsed.hasNext) {
        tags ++ fetchTags(parsed.next)
      } else tags
    }

    fetchTags("https://registry.hub.docker.com/v2/repositories/uxforms/local-formexecutor/tags")
  }

  def findMatchingImage(dslVersion: String): Option[String] = {
    val tags = findAllTags()
    SemanticVersion(dslVersion).flatMap(dv => selectTag(tags, dv).map(_.name))
  }

  def selectTag(tags: Set[Tag], dslVersion: SemanticVersion): Option[Tag] = {
    Try(tags.filter(_.dslVersion == dslVersion).maxBy(_.formexecutorVersion)).toOption
  }

}