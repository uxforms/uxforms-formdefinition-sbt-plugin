package com.uxforms.sbt

import scala.util.Try

case class SemanticVersion(major: Short, minor: Short, patch: Short) extends Ordered[SemanticVersion] {

  override def compare(that: SemanticVersion): Int = {
    major.compare(that.major) match {
      case 0 => minor.compare(that.minor) match {
        case 0 => patch.compareTo(that.patch)
        case x => x
      }
      case x => x
    }
  }

  override def toString: String = s"$major.$minor.$patch"
}

object SemanticVersion {
  def apply(dotSeparated: String): Option[SemanticVersion] = {
    Try {
      val parts = dotSeparated.split('.')
      SemanticVersion(parts(0).toShort, parts(1).toShort, parts(2).toShort)
    }.toOption
  }
}