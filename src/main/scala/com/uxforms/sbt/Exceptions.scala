package com.uxforms.sbt

case object PermissionDeniedException extends RuntimeException("Unauthorised. Check your api key, reload the project, and try again.")

case class FormAlreadyDeployedException(name: String, version: String) extends RuntimeException(s"There is already a form called $name with version $version deployed")

case class InvalidFormFileException(msg: String) extends RuntimeException(s"Invalid form file: $msg")
