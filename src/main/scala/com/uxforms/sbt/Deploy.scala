package com.uxforms.sbt

import java.util.Properties

import sbt.{File, Logger}

object Deploy {

  import dispatch._
  import Defaults._

  import scala.concurrent.duration._

  def apply(deployProperties: Properties, bundleFile: File, name: String, version: String)(logger: Logger): Unit = {
    val url = s"${deployProperties.getProperty("host")}/bundles"
    logger.debug(s"Posting $bundleFile to $url")

    val deployerUrl = dispatch.url(url)
      .addHeader("Authorization", s"""UXFORMS-TOKEN apikey="${deployProperties.getProperty("apikey")}"""")
      .addHeader("Content-Type", "application/java-archive")
    val response = scala.concurrent.Await.result(Http.default(deployerUrl.setBody(bundleFile.getAbsoluteFile).POST), 60.seconds)
    response.getStatusCode match {
      case 200 => logger.info(s"Successfully deployed $name version $version to $url")
      case 401 => throw PermissionDeniedException
      case 409 => throw FormAlreadyDeployedException(name, version)
      case c@_ => logger.error(s"Returned $c with ${response.getResponseBody()} from $url")
    }

  }
}
