package com.uxforms.sbt

import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.sbt.osgi.{OsgiKeys, SbtOsgi}
import com.uxforms.sbt.FormDefinitionPlugin.autoImport.{UXForms, UXForms2}
import sbt.Keys._
import sbt._

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, StandardCopyOption}
import java.util.Properties
import scala.collection.JavaConverters._
import scala.concurrent.duration.Duration

object FormDefinitionPlugin extends AutoPlugin {

  override def requires: Plugins = SbtOsgi

  object autoImport {
    val formDefinitionName = settingKey[String]("This will be the path on which your form will be deployed.")
    val formDefinitionClass = settingKey[String]("The entry point to your form. I.e. the fully qualified name of your object that extends FormDefinitionFactory")
    val activateOnDeploy = settingKey[Boolean]("When deploying a form also make it the active version")
    val forceDelete = settingKey[Boolean]("When deleting a form, delete it even if it is the last version remaining")
    val excludeJarPrefixes = settingKey[Set[String]]("A Set of jar name prefixes to be excluded from the fat jar. Initially set to be uxforms-dsl and its transitive dependencies as they are automatically placed on the form's classpath at runtime")
    val themeName = settingKey[String]("The theme name to be reported to the form-deployer API")
    val retentionPeriod = settingKey[Duration]("The retention period to be reported to the form-deployer API")

    lazy val UXForms = config("uxforms") extend Compile
    lazy val UXForms2 = config("uxforms2") extend Compile
  }

  object manualImport {
    val deploy = taskKey[Unit]("deploy form")
    val undeploy = taskKey[Boolean]("undeploy form")

    val deployProperties = UXForms / settingKey[Properties]("Where to deploy/undeploy forms and with which permissions")
    val propertiesFileLocation = UXForms / settingKey[String]("The path to locate the file containing UX Forms hostname and credentials")

    val pluginConfigLocation = UXForms2 / settingKey[String]("The path to locate the configuration file containing UX Forms' hostname and credentials")
    val pluginConfig = UXForms2 / settingKey[Config]("Where to deploy/undeploy forms, and supporting configuration to run forms locally")

    val deployToEnvironmentName = UXForms2 / settingKey[String]("The name of the environment that the deploy and undeploy tasks should be run against. Defaults to babbage")
    val debugPort = UXForms2 / settingKey[Short]("The port on which the remote debugger will be enabled. Defaults to 5000")
    val runPort = UXForms2 / settingKey[Short]("The port on which your form will run. Defaults to 9000")
    val runPortHttps = UXForms2 / settingKey[Short]("The https port on which your form will run. Defaults to 9443")
    val runAsHttps = UXForms2 / settingKey[Boolean]("Whether to run locally over http or https. Https requires you install UX Forms' development certificate, see See https://uxforms.com/documentation/2.0/running-forms-locally for instructions. Defaults to true. To run the form via http either set this value in your build.sbt or run this command: set FormDefinitionPlugin.manualImport.runAsHttps := false;")
    val jmxPort = UXForms2 / settingKey[Short]("The port on which jmx will be exposed. Defaults to 8004")
    val staticAssetsPort = UXForms2 / settingKey[Short]("The port on which your static assets / theme will be hosted. Defaults to 9080")
    val staticAssetsPortHttps = UXForms2 / settingKey[Short]("The https port on which your static assets / theme will be hosted. Defaults to 9083")
    val dockerComposePrepare = UXForms2 / taskKey[File]("Create the docker-compose.yml to run this form locally")
    val dockerComposeRun = UXForms2 / taskKey[Unit]("Delegates to docker-compose to run this form and all of its runtime dependencies")
    val dockerComposeRestart = UXForms2 / taskKey[Unit]("Delegates to docker-compose to restart the docker container running this form")
    val dockerComposeStop = UXForms2 / taskKey[Unit]("Delegates to docker-compose to stop running this form and all of its runtime dependencies")
  }

  import autoImport._
  import manualImport._

  lazy val baseUXFormsSettings: Seq[Def.Setting[_]] = Seq(
    propertiesFileLocation := System.getProperty("user.home") + "/.bundleDeployerCredentials",
    deployProperties := DeployProperties(propertiesFileLocation.value)(sLog.value),
    undeploy := Undeploy(deployProperties.value, formDefinitionName.value, OsgiKeys.bundleVersion.value)(streams.value.log),
    deploy := {
      val successfullyUndeployed = undeploy.value
      if (successfullyUndeployed) {
        (streams.value: @sbtUnchecked).log.debug("Waiting for form to be deleted...")
        Thread.sleep(5001)
      }
      Deploy(deployProperties.value, OsgiKeys.bundle.value.getAbsoluteFile, formDefinitionName.value, OsgiKeys.bundleVersion.value)(streams.value.log)
    }
  )

  private val transitiveUxformsDslDependencies: Set[String] = Set(
    "uxforms-dsl",
    "i8n-1.5.3",
    "emailaddress_2.11-2.1.0",
    "jbcrypt-0.3m",
    "jsr305-3.0.0",
    "mustache_2.11-1.3",
    "scala-uri_2.11-0.4.10",
    "dispatch-core_2.11-0.11",
    "async-http-client-1.9.38",
    "play-json_2.11-2.4.3",
    "scaffeine_2.11-1.0.0",
    "icu4j-67.1",
    "parboiled_2.11-2.1.0",
    "scala-xml_2.11-1.0.1",
    "netty-3.10.5.Final",
    "slf4j-api-1.7.12",
    "play-iteratees_2.11-2.4.3",
    "play-functional_2.11-2.4.3",
    "play-datacommons_2.11-2.4.3",
    "joda-time-2.8.1",
    "joda-convert-1.7",
    "jackson-core-2.5.4",
    "jackson-annotations-2.5.4",
    "jackson-databind-2.5.4",
    "jackson-datatype-jdk8-2.5.4",
    "jackson-datatype-jsr310-2.5.4",
    "caffeine-2.2.2",
    "scala-java8-compat_2.11-0.7.0",
    "shapeless_2.11-2.1.0",
    "scala-stm_2.11-0.7",
    "config-1.3.0"
  )

  /**
   * Logic taken from the Assembly plugin: https://github.com/sbt/sbt-assembly/blob/develop/src/main/scala/sbtassembly/Assembly.scala
   */
  private def excludedScalaJars(scalaVersionNumber: VersionNumber) = {
    val isScala213AndLater = scalaVersionNumber.numbers.length >= 2 && scalaVersionNumber._1.get >= 2 && scalaVersionNumber._2.get >= 13
    if (isScala213AndLater) {
      Set("scala-actors",
        "scala-compiler",
        "scala-continuations",
        "scala-library",
        "scala-reflect")
    } else {
      Set(
        "scala-actors",
        "scala-compiler",
        "scala-continuations",
        "scala-library",
        "scala-parser-combinators",
        "scala-reflect",
        "scala-swing",
        "scala-xml"
      )
    }
  }

  private def filteredExternalDependencies(externalClasspath: Classpath, prefixesToExclude: Set[String]): Seq[Attributed[File]] =
    externalClasspath.filterNot(af => prefixesToExclude.exists(prefix => af.data.getName.startsWith(prefix)))

  lazy val baseUXFormsSettings2: Seq[Def.Setting[_]] = Seq(
    pluginConfigLocation := System.getProperty("user.home") + "/.uxforms/sbt-uxforms.conf",
    pluginConfig := ConfigFactory.parseFile(new File(pluginConfigLocation.value)),
    Compile / packageBin / packageOptions += Package.ManifestAttributes(
      "FormDefinitionFactory-Class" -> formDefinitionClass.value,
      "FormDefinitionDSL-Version" -> libraryVersion("com.uxforms", "uxforms-dsl", libraryDependencies.value),
      "FormDefinitionRetention-Period" -> retentionPeriod.value.toMillis.toString,
      "FormDefinitionTheme-Name" -> themeName.value
    ),
    excludeJarPrefixes := excludedScalaJars(VersionNumber(scalaVersion.value)) ++ transitiveUxformsDslDependencies ++ Set("slf4j-api"),
    Compile / packageBin / mappings ++= filteredExternalDependencies((Compile / externalDependencyClasspath).value, excludeJarPrefixes.value).map(f => (f.data -> f.data.getName)),
    deployProperties := DeployProperties(propertiesFileLocation.value)(sLog.value),
    activateOnDeploy := true,
    forceDelete := true,
    deployToEnvironmentName := "babbage",
    undeploy := {
      val envConf = pluginConfig.value.getConfig(s"deploy.${deployToEnvironmentName.value}")
      Undeploy2(envConf.getString("host"), envConf.getString("apikey"), formDefinitionName.value, version.value, forceDelete.value)(streams.value.log)
    },
    deploy := {
      undeploy.value
      val uberJar = (Compile / packageBin).value
      val envConf = pluginConfig.value.getConfig(s"deploy.${deployToEnvironmentName.value}")
      Deploy2(envConf.getString("host"), envConf.getString("apikey"), uberJar, formDefinitionName.value, version.value, activateOnDeploy.value)(streams.value.log)
    },
    debugPort := 5005,
    runPort := 9000,
    runPortHttps := 9443,
    runAsHttps := true,
    jmxPort := 8004,
    staticAssetsPort := 9080,
    staticAssetsPortHttps := 9083,
    dockerComposePrepare := {
      (Compile / packageBin).value
      val themeConfig = pluginConfig.value.getConfigList("themes").asScala
      val config = DockerConfig(
        formDefinitionName.value,
        ImageFinder.findMatchingImage(libraryVersion("com.uxforms", "uxforms-dsl", libraryDependencies.value)).get,
        debugPort.value,
        runPort.value,
        runPortHttps.value,
        runAsHttps.value,
        jmxPort.value,
        (Compile / packageBin / artifactPath).value.getName,
        staticAssetsPort.value,
        staticAssetsPortHttps.value,
        formDefinitionClass.value,
        (target.value / s"scala-${scalaBinaryVersion.value}").getAbsolutePath,
        csrCacheDirectory.value.getAbsolutePath,
        (ivyConfiguration / ivyPaths).value.ivyHome.map(_.getAbsolutePath) ,
        filteredExternalDependencies((Compile / externalDependencyClasspath).value, excludeJarPrefixes.value).map(_.data),
        themeConfig.map(c => ThemeMapping(c.getString("name"), c.getString("location"))).toSet
      )

      val compose = DockerComposer.buildDockerComposeYaml(config)
      val targetDir = target.value.toPath / "uxforms"
      Files.createDirectories(targetDir)
      Files.copy(FormDefinitionPlugin.getClass.getClassLoader.getResourceAsStream("static-content.conf"), (targetDir / "static-content.conf"), StandardCopyOption.REPLACE_EXISTING)
      Files.copy(FormDefinitionPlugin.getClass.getClassLoader.getResourceAsStream("form-frontend.conf"), (targetDir / "form-frontend.conf"), StandardCopyOption.REPLACE_EXISTING)
      Files.copy(FormDefinitionPlugin.getClass.getClassLoader.getResourceAsStream("localhost.pem"), (targetDir / "localhost.pem"), StandardCopyOption.REPLACE_EXISTING)
      Files.copy(FormDefinitionPlugin.getClass.getClassLoader.getResourceAsStream("localhost-key.pem"), (targetDir / "localhost-key.pem"), StandardCopyOption.REPLACE_EXISTING)
      Files.copy(FormDefinitionPlugin.getClass.getClassLoader.getResourceAsStream("error.html"), (targetDir / "error.html"), StandardCopyOption.REPLACE_EXISTING)
      Files.write(targetDir / "docker-compose.yml", compose.getBytes(StandardCharsets.UTF_8)).toFile
    },
    dockerComposeRun := {
      dockerComposePrepare.value
      import scala.sys.process.stringSeqToProcess
      val dockerComposePath = (target.value.toPath / "uxforms" / "docker-compose.yml").toFile.getAbsolutePath
      Seq("docker-compose", "-f", dockerComposePath, "up", "-V", "--detach").!
      if (runAsHttps.value) {
        (streams.value: @sbtUnchecked).log.info(s"Form up and running on https://localhost:${runPortHttps.value}/${formDefinitionName.value}")
        (streams.value: @sbtUnchecked).log.info("See https://uxforms.com/documentation/2.0/running-forms-locally for instructions on how to trust the self-signed certificate. Alternatively, set Uxforms2/runAsHttps to false to run just on http")
      } else {
        (streams.value: @sbtUnchecked).log.info(s"Form up and running on http://localhost:${runPort.value}/${formDefinitionName.value}")
        (streams.value: @sbtUnchecked).log.info("Set Uxforms2/runAsHttps to true if you'd prefer to run on https")
      }
    },
    dockerComposeRestart := {
      dockerComposePrepare.value
      import scala.sys.process.stringSeqToProcess
      val dockerComposePath = (target.value.toPath / "uxforms" / "docker-compose.yml").toFile.getAbsolutePath
      Seq("docker-compose", "-f", dockerComposePath, "restart", "form").!
    },
    dockerComposeStop := {
      import scala.sys.process.stringSeqToProcess
      val dockerComposePath = (target.value.toPath / "uxforms" / "docker-compose.yml").toFile.getAbsolutePath
      Seq("docker-compose", "-f", dockerComposePath, "stop").!
    }
  )


  override def projectSettings: Seq[Def.Setting[_]] =
    SbtOsgi.autoImport.osgiSettings ++
      inConfig(UXForms)(baseUXFormsSettings) ++
      inConfig(UXForms2)(baseUXFormsSettings2) ++
      Seq(
        OsgiKeys.bundleSymbolicName := formDefinitionName.value,
        resolvers ++= Seq(
          "uxforms-public" at "https://artifacts-public.uxforms.net"
        ),
        libraryDependencies ++= Seq(
          "com.uxforms" %% "uxforms-dsl" % "15.27.0" % Provided,
          "org.osgi" % "org.osgi.core" % "5.0.0" % Provided
        ),
        formDefinitionName := name.value
      )

  private def libraryVersion(org: String, lib: String, dependencies: Seq[ModuleID]): String =
    dependencies
      .find(d => d.organization == org && d.name == lib)
      .fold(s"Library '$org' % '$lib' not found")(_.revision)
}


object BooleanHelper {
  def toNiceString: Boolean => String = { x =>
    if (x) "true" else "false"
  }
}
