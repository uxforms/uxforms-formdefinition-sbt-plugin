package com.uxforms.sbt

import java.util.Properties

import sbt.Logger

object Undeploy {

  import aQute.bnd.osgi.Analyzer
  import dispatch._
  import Defaults._

  import scala.concurrent.duration._

  def apply(deployProperties: Properties, name: String, version: String)(logger: Logger): Boolean = {
    val versionToDelete = Analyzer.cleanupVersion(version)
    val url = s"${deployProperties.getProperty("host")}/bundles/$name/$versionToDelete"
    logger.debug(s"Deleting $url")

    val deployerUrl = dispatch.url(url).addHeader("Authorization", s"""UXFORMS-TOKEN apikey="${deployProperties.getProperty("apikey")}"""")
    val response = scala.concurrent.Await.result(Http.default(deployerUrl.DELETE), 60.seconds)
    response.getStatusCode match {
      case 200 =>
        logger.info(s"Successfully deleted $name version $versionToDelete from $url")
        true
      case 401 => throw PermissionDeniedException
      case 404 =>
        logger.warn(s"No such form $name with version $versionToDelete on $url")
        false
      case c@_ =>
        logger.error(s"Returned $c with ${response.getResponseBody()} from $url")
        false
    }
  }
}
