package com.uxforms.sbt


import com.github.mustachejava.DefaultMustacheFactory
import org.apache.commons.io.FilenameUtils.separatorsToUnix
import sbt.File

import java.io.StringWriter
import scala.collection.JavaConverters.*

object DockerComposer {

  private val mf = new DefaultMustacheFactory()

  def buildDockerComposeYaml(dockerConfig: DockerConfig): String = {
    val compiled = mf.compile("docker-compose.mustache")
    val writer = new StringWriter()
    compiled.execute(writer, dockerConfig.asArgs)
    writer.toString
  }

}

case class DockerConfig(formDefinitionName: String,
                         formexecutorImageVersion: String,
                        debugPort: Short,
                        runPort: Short,
                        runPortHttps: Short,
                        runAsHttps: Boolean,
                        jmxPort: Short,
                        formDefinitionJarName: String,
                        staticAssetsPort: Short,
                        staticAssetsPortHttps: Short,
                        formDefinitionFactoryClassName: String,
                        projectTargetDir: String,
                        coursierCacheDir: String,
                        ivyCacheDir: Option[String],
                        formDefinitionLibs: Seq[File],
                        themes: Set[ThemeMapping]) {

  import java.util.Map as JMap

  lazy val asArgs: JMap[String, Any] = {
    (
      Map(
        "formDefinitionName" -> formDefinitionName,
        "formexecutorImageVersion" -> formexecutorImageVersion,
        "debugPort" -> debugPort,
        "runPort" -> runPort,
        "runPortHttps" -> runPortHttps,
        "runAsHttps" -> runAsHttps,
        "jmxPort" -> jmxPort,
        "formDefinitionJarName" -> formDefinitionJarName,
        "staticAssetsPort" -> staticAssetsPort,
        "staticAssetsPortHttps" -> staticAssetsPortHttps,
        "formDefinitionFactoryClassName" -> formDefinitionFactoryClassName,
        "projectTargetDir" -> projectTargetDir,
        "coursierCacheDir" -> coursierCacheDir,
        "formDefinitionLibs" -> formDefinitionLibs.zipWithIndex.map(l => {
          (l, ivyCacheDir) match {
            case ((f, i), Some(d)) if f.getAbsolutePath.startsWith(d) => Map("index" -> (i + 1), "path" -> separatorsToUnix(f.getAbsolutePath.stripPrefix(d)), "isIvy" -> true).asJava
            case ((f, i), _) => Map("index" -> (i + 1), "path" -> separatorsToUnix(f.getAbsolutePath.stripPrefix(coursierCacheDir)), "isIvy" -> false).asJava
          }
        }).asJava,
        "themes" -> themes.map(t => Map("name" -> t.name, "location" -> t.location).asJava).asJava
      ) ++ ivyCacheDir.map(d => "ivyCacheDir" -> d)
      ).asJava
  }

}

case class ThemeMapping(name: String, location: String)
