# Building

Building `form-frontend`'s custom error.html page is a bit involved, so here's how to do it.

Download https://uxforms.com's error page into a single .html file: 
```
docker run capsulecode/singlefile "https://uxforms.com/whoops" > my_error.html
```
This gives us a single file will all its external assets inlined into it. This way it display as
expected even when viewed offline.

Then:

1. Remove extraneous information:
   2. Analytics js
   3. Copyright symbol and year from footer
   3. Cookie banner
4. Add in dynamic html fragments using Server Side Includes from nginx for <title> and body content
5. Add polling js to reload the page once the form is ready