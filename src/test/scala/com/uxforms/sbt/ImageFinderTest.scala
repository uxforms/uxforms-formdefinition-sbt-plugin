package com.uxforms.sbt

import com.uxforms.sbt.ImageFinder.*
import utest.*

object ImageFinderTest extends TestSuite {

  override val tests: Tests = Tests {

    test("selectTag") {

      test("should return None when there are no tags with our dsl version") {
        val tags = Set(Tag("16.0.415-dsl.15.23.0", SemanticVersion(16, 0, 415), SemanticVersion(15, 23, 0)))
        val result = selectTag(tags, SemanticVersion(16, 0, 415))
        assert(result.isEmpty)
      }

      test("should return the highest formexecutor version when there is more than one for the dsl version") {
        val tags = Set(
          Tag("16.0.415-dsl.15.23.0", SemanticVersion(16, 0, 415), SemanticVersion(15, 23, 0)),
          Tag("16.0.416-dsl.15.23.0", SemanticVersion(16, 0, 416), SemanticVersion(15, 23, 0)),
          Tag("16.0.417-dsl.15.24.0", SemanticVersion(16, 0, 417), SemanticVersion(15, 24, 0))
        )
        val result = selectTag(tags, SemanticVersion(15, 23, 0))
        assert(result.exists(_.formexecutorVersion == SemanticVersion(16, 0, 416)))
      }
    }
  }

}
