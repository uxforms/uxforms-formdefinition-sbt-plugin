package com.uxforms.sbt

import utest._

object SemanticVersionTest extends TestSuite {

  val tests = Tests {

    test("ordering") {
      test("major number should take precedence") {
        val four = SemanticVersion(4, 2, 3)
        val five = SemanticVersion(5, 2, 3)
        assert(four < five)
        assert(!(four == five))
        assert(!(four > five))
      }
      test("minor number should take precedence when majors are the same") {
        val point2 = SemanticVersion(4, 2, 5)
        val point3 = SemanticVersion(4, 3, 5)

        assert(point2 < point3)
        assert(!(point2 == point3))
        assert(!(point2 > point3))
      }
      test("patch number should take precedence when majors and minors are the same") {
        val p3 = SemanticVersion(1, 2, 3)
        val p6 = SemanticVersion(1, 2, 6)

        assert(p3 < p6)
        assert(!(p3 == p6))
        assert(!(p3 > p6))
      }
      test("versions should be equal when all members are equal") {
        val a = SemanticVersion(1, 2, 3)
        val b = SemanticVersion(1, 2, 3)
        assert(a == b)
        assert(!(a != b))
      }
    }

    test("apply from string") {
      test("should parse a simple three-part string") {
        val result = SemanticVersion("1.2.3")
        assert(result.contains(SemanticVersion(1, 2, 3)))
      }
      test("should return None if it can't be parsed") {
        val result = SemanticVersion("whoops")
        assert(result.isEmpty)
      }
    }

    test("toString") {
      test("should print a simple three-part string") {
        val result = SemanticVersion(10, 22, 45).toString
        assert(result == "10.22.45")
      }
    }
  }

}
