package com.uxforms.sbt

import org.mockito.MockitoSugar.*
import utest.*

import java.util.{List as JList, Map as JMap}
import scala.jdk.CollectionConverters.{collectionAsScalaIterableConverter, mapAsScalaMapConverter}

object DockerConfigTest extends TestSuite {

  private def mockFile(path: String): sbt.File = {
    val f = mock[sbt.File]
    when(f.getAbsolutePath).thenReturn(path)
    f
  }

  override def tests: Tests = Tests {
    "asArgs" - {
      "formDefinitionLibs" - {
        "should force the path to have unix separators as it's used inside the container" - {
          val args = DockerConfig("", "", 1, 2, 3, true, 4, "", 5, 6, "", "", "", None, Seq(
            mockFile("/unixFile"),
            mockFile("\\windowsFile")
          ), Set.empty).asArgs.asScala

          val libs = args("formDefinitionLibs").asInstanceOf[JList[JMap[String, Any]]].asScala.toList

          val first = libs.head.asScala

          assert(
            first("index") == 1,
            first("path") == "/unixFile"
          )

          val second = libs(1).asScala

          assert(
            second("index") == 2,
            second("path") == "/windowsFile"
          )
        }
      }
    }

  }
}
