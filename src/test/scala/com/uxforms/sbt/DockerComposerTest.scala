package com.uxforms.sbt

import com.uxforms.sbt.DockerComposer.buildDockerComposeYaml
import org.yaml.snakeyaml.Yaml
import sbt.File
import utest.*

import java.util.Map as JMap
import scala.collection.JavaConverters.*

object DockerComposerTest extends TestSuite {

  private val config = DockerConfig(
    "my-form-name",
    "16.0.14-dsl.15.18.1",
    5000,
    9000,
    9443,
    false,
    8004,
    "my-form-0.0.1-SNAPSHOT.jar",
    9080,
    9083,
    "com.example.myproject.myform.MyFormDefinitionFactory",
    "./target/scala-2.11",
    "",
    None,
    Seq.empty,
    Set(
      ThemeMapping("my-theme", "~/code/themes/my-theme/target")
    )
  )

  private def parseGeneratedYaml(dc: DockerConfig): Map[String, Any] = {
    val jmap = new Yaml().load(buildDockerComposeYaml(dc)).asInstanceOf[JMap[String, Any]]
    mapAsScalaMap(jmap).toMap
  }

  private def parseForm(m: Map[String, Any]): Form = {
    val services = mapAsScalaMap(
      m("services").asInstanceOf[JMap[String, Any]]
    )
    val formEntry = mapAsScalaMap(
      services("form").asInstanceOf[JMap[String, Any]]
    )
    Form(
      formEntry("image").asInstanceOf[String],
      formEntry("ports").asInstanceOf[java.util.ArrayList[String]].asScala.toSet,
      formEntry("volumes").asInstanceOf[java.util.ArrayList[String]].asScala.toSet,
      formEntry("entrypoint").asInstanceOf[String],
      formEntry("command").asInstanceOf[String])
  }

  private def parseFormFrontend(m: Map[String, Any]): FormFrontend = {
    val services = mapAsScalaMap(
      m("services").asInstanceOf[JMap[String, Any]]
    )
    val formFrontendEntry = mapAsScalaMap(
      services("form-frontend").asInstanceOf[JMap[String, Any]]
    )
    FormFrontend(
      formFrontendEntry("image").asInstanceOf[String],
      formFrontendEntry("ports").asInstanceOf[java.util.ArrayList[String]].asScala.toSet,
      formFrontendEntry("volumes").asInstanceOf[java.util.ArrayList[String]].asScala.toSet
    )
  }

  override def tests: Tests = Tests {

    val result = parseGeneratedYaml(config)

    val form: Form = parseForm(result)
    val formFrontend: FormFrontend = parseFormFrontend(result)

    def nginx(serviceName: String): NGinx = {
      val services = mapAsScalaMap(
        result("services").asInstanceOf[JMap[String, Any]]
      )
      val serviceEntry = mapAsScalaMap(
        services(serviceName).asInstanceOf[JMap[String, Any]]
      )
      NGinx(
        serviceEntry("image").asInstanceOf[String],
        serviceEntry("volumes").asInstanceOf[java.util.ArrayList[String]].asScala.toSet,
        serviceEntry.get("expose").map(_.asInstanceOf[java.util.ArrayList[String]].asScala.toSet).getOrElse(Set.empty),
        serviceEntry.get("ports").map(_.asInstanceOf[java.util.ArrayList[String]].asScala.toSet).getOrElse(Set.empty)
      )
    }

    val staticContent = nginx("static-content")

    test("formExecutorImage") {
      assert(form.image == "uxforms/local-formexecutor:16.0.14-dsl.15.18.1")
    }

    test("debugPort") {
      assert(form.ports.contains(s"${config.debugPort}:5005"))
      assert(form.command.contains(s"-jvm-debug 5005"))
    }

    test("runPort") {
      assert(formFrontend.ports.contains(s"${config.runPort}:80"))
      assert(formFrontend.ports.contains(s"${config.runPortHttps}:443"))
    }

    test("formDefinitionJarName") {
      assert(form.command.contains(s"-Dformdefinition.classpath.0=/opt/docker/formdefinition/formdef/${config.formDefinitionJarName}"))
    }

    test("staticAssetsPort") {
      assert(form.command.contains(s"-DstaticAssetsBaseURL=http://localhost:${config.staticAssetsPort}"))
      assert(staticContent.ports == Set(
        s"${config.staticAssetsPort}:80",
        s"${config.staticAssetsPortHttps}:443"
      ))
    }

    test("formDefinitionFactoryClassName") {
      assert(form.command.contains(s"-Dfdf.name=${config.formDefinitionFactoryClassName}"))
    }

    test("projectTargetDir") {
      assert(form.volumes.contains(s"${config.projectTargetDir}:/opt/docker/formdefinition/formdef"))
    }

    test("conditionally write ivyCacheDir volume") {
      assert(!form.volumes.exists(_.contains("opt/docker/formdefinition/ivy")))
      val configWithIvy = config.copy(ivyCacheDir = Some("/home/user/.ivy2"))
      val formWithIvy = parseForm(parseGeneratedYaml(configWithIvy))
      assert(formWithIvy.volumes.exists(_.contains("opt/docker/formdefinition/ivy")))
    }

    test("empty libs should not write extra classpath entries") {
      assert(!form.command.contains("formdefinition.classpath.1"))
    }

    test("libs should write extra classpath entries") {
      val libFile1 = new File("first-lib.jar")
      val libFile2 = new File("second-lib.jar")
      val configWithLibs = config.copy(formDefinitionLibs = Seq(libFile1, libFile2))
      val parsedConfigWithLibs = parseGeneratedYaml(configWithLibs)
      val form = parseForm(parsedConfigWithLibs)
      assert(form.command.contains("formdefinition.classpath.1"))
      assert(form.command.contains("formdefinition.classpath.2"))
    }

    test("libs in local ivy cache should be resolved too") {
      val coursierDir = "/home/user/coursier"
      val coursierFile = new File(coursierDir + "/file1.jar")
      val ivyDir = "/home/user/ivy"
      val ivyFile = new File(ivyDir + "/file2.jar")
      val configWithLibs = config.copy(formDefinitionLibs = Seq(coursierFile, ivyFile), coursierCacheDir = coursierDir, ivyCacheDir = Some(ivyDir))
      val formWithLibs = parseForm(parseGeneratedYaml(configWithLibs))
      assert(formWithLibs.command.contains("/opt/docker/formdefinition/lib/file1.jar"))
      assert(formWithLibs.command.contains("/opt/docker/formdefinition/ivy/file2.jar"))
    }

    test("conditionally write static content url") {
      val configWithHttps = config.copy(runAsHttps = true, staticAssetsPortHttps = 9083)
      val formWithHttpsContent = parseForm(parseGeneratedYaml(configWithHttps))
      assert(formWithHttpsContent.command.contains("-DstaticAssetsBaseURL=https://localhost:9083"))

      val configWithoutHttps = config.copy(runAsHttps = false, staticAssetsPortHttps = 9080)
      val formWithoutHttsContent = parseForm(parseGeneratedYaml(configWithoutHttps))
      assert(formWithoutHttsContent.command.contains("-DstaticAssetsBaseURL=http://localhost:9080"))
    }
  }
}

case class Form(image: String, ports: Set[String], volumes: Set[String], entrypoint: String, command: String)

case class FormFrontend(image: String, ports: Set[String], volumes: Set[String])

case class NGinx(image: String, volumes: Set[String], expose: Set[String], ports: Set[String])