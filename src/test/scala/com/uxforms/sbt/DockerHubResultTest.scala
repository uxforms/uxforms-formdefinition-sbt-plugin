package com.uxforms.sbt

import utest.*

object DockerHubResultTest extends TestSuite {

  override val tests: Tests = Tests {

    test("tag") {

      test("parse valid tag") {
        val tag = DockerHubResult("1.2.3-dsl.4.5.6").tag
        assert(tag.get == Tag("1.2.3-dsl.4.5.6", SemanticVersion(1, 2, 3), SemanticVersion(4, 5, 6)))
      }

      test("ignore SNAPSHOT versions") {
        val tag = DockerHubResult("15.5.18-SNAPSHOT-dsl.15.16.0").tag
        assert(tag.isEmpty)
      }
    }
  }
}
