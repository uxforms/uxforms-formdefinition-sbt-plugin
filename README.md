# uxforms-formdefinition-sbt-plugin

An SBT plugin to simplify making form definitions.

The version of this plugin determines the version of UX Forms that your form will be deployed against.

It:

* Makes the SbtOsgi plugin available to make creating an OSGi bundle more simple
* Provides a more useful shell prompt than sbt's default
* Adds `"org.osgi" % "org.osgi.core" % "5.0.0" % Provided` and `"com.uxforms" %% "uxforms-dsl" % "15.27.0" % Provided` to your project's dependencies
* By default sets the form's deployment path to be the same as the project's `name`


## Usage

In your form definition's `plugins.sbt`:

If using SBT version 0.13.11 or higher:

```resolvers += "uxforms-public" at "https://artifacts-public.uxforms.net"```
   
Alternatively if using SBT version 0.13.10 or lower:

```resolvers += Resolver.url("uxforms-public", url("https://artifacts-public.uxforms.net"))(Resolver.mavenStylePatterns)```

Then:

```addSbtPlugin("com.uxforms" % "sbt-formdefinition-plugin" % "3.8.5")```

### Which version should I use:

* Version 2.7.0 - for use with sbt 0.13.
* Version 3.0.0 and above - for use with sbt 1.14.1 and above.


In your form definition's `build.sbt`:

    enablePlugins(SbtOsgi, FormDefinitionPlugin)

    name := "hello-world-questionnaire"

    formDefinitionClass := "com.example.form.HelloWorldQuestionnaireFormDefinitionFactory"

By default your form will be deployed on the url path taken from your project's `name`. To deploy to a path _other_ than your project's `name`, add this to your `build.sbt`: 

    formDefinitionName := "my-different-form-name"
    
# UX Forms 1.0

## Tasks

* `osgiBundle`: Generates the OSGi bundle to be deployed to UX Forms. See the [osgi plugin](https://github.com/sbt/sbt-osgi) for more details and how to customise its contents.

* `Uxforms / deploy` Deploys the current form to UX Forms.

* `Uxforms / undeploy` Undeploys the current form from UX Forms.


## Configuration

The `Uxforms / deploy` and `Uxforms / undeploy` tasks assume a file containing the host to deploy to and your 
api key can be found at `~/.bundleDeployerCredentials`. (This can be overridden by changing the `propertiesFileLocation` setting).

This file is expected to contain:

```
host=https://deployer.babbage.uxforms.com
apikey=your-apikey-from-dashboard
```

# UX Forms 2.0

## Tasks

* `package` Generates the jar to be deployed to UX Forms.

* `Uxforms2 / dockerComposeRun` Runs the current form on your local machine via DockerCompose.
* `Uxforms2 / dockerComposeRestart` Restarts only the previously running form container. Perhaps most usefully can be run continuously (i.e. `~Uxforms2/dockerComposeRestart`) whilst you're working on the form to always have the latest changes running in your browser.
* `Uxforms2 / dockerComposeStop` Stops all containers used to run your form locally

* `Uxforms2 / deploy` Deploys the current form to UX Forms. Reads the `activateOnDeploy` setting to decide whether to also activate the form. This defaults to `true`.

* `Uxforms2 / undeploy` Undeploys the current form from UX Forms. Reads the `forceDelete` setting to decide whether to delete the form even if there are no other versions deployed with the same name. This defaults to `true`.

## Configuration

The `Uxforms2 / deploy` and `Uxforms2 / undeploy` tasks assume a file containing the host to deploy to and your 
api key can be found at `~/.uxforms/sbt-uxforms.conf`. (This can be overridden by changing the `pluginConfigLocation` setting).

This file is expected to contain:

```
deploy {
  environment-name {
    host="https://deployer.environment.uxforms.net"
    apikey=redacted
  }
}
themes: [
  {
    name=my-theme-name
    location=/path/to/built/theme/contents/on/your/machine 
  }
]
```

See our [online technical documentation](https://uxforms.com/documentation/2.0/) for further details and usage instructions.